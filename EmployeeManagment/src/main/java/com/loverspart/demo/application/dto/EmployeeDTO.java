package com.loverspart.demo.application.dto;

import com.loverspart.demo.application.domain.Employee;

public class EmployeeDTO {

	private long employeeId;
	private String firstName;
	private String lastName;
	private String email;

	public EmployeeDTO(long employeeId, String firstName, String lastName, String email) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EmployeeDTO valueOf(Employee employeeDTO) {
		return new EmployeeDTO(employeeDTO.getEmployeeId(), employeeDTO.getFirstName(), employeeDTO.getLastName(),
				employeeDTO.getEmail());
	}

}
